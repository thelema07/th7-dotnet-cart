# README

Th7 Dotnet Cart
A Testing Web API, designed in Net Core V 3.0

### What is this repository for?

- Quick summary

A Bookstore API made to login users, Get a Collection of books
and logged in users can push items to a cart

- Version

  1.0.0

### How do I get set up?

- Execute "dotnet run" in the api folder to activate the backend
- It can be tested in port: 3007

- How to install Entity Framework
- Install Microsoft.EntityFrameworkCore.Design , The solution of the compatibility "dotnet ef" is on the web

- Notes about NetCore Setup
- App created on version 3.1, some ef may need to setup correctly an environment variable.

### Contribution guidelines

- Writing tests
- Code review
- Other guidelines

### Who do I talk to?

- Daniel Mesu - Software Developer
- LinkedIn: https://www.linkedin.com/in/daniel-mesu-92938871/
- Mail: th7.design@gmail.com
