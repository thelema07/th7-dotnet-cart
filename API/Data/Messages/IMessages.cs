using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Models;

namespace API.Data.Messages
{
    public interface IMessages
    {
        Task<IEnumerable<Message>> GetMessages();
        Task<Message> GetMessage(Guid id);
    }
}