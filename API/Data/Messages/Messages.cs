using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Models;
using Microsoft.EntityFrameworkCore;

namespace API.Data.Messages
{
    public class Messages : IMessages
    {
        private readonly DataContext _context;
        public Messages(DataContext context)
        {
            _context = context;
        }

        public async Task<Message> GetMessage(Guid id)
        {
            var message = await _context.Messages.FirstOrDefaultAsync(m => m.Id == id);

            if (message == null) return null;

            return message;
        }

        public async Task<IEnumerable<Message>> GetMessages()
        {
            var messages = await _context.Messages.ToListAsync();

            return messages;
        }

        public Task<Message> PostMessage()
        {
            throw new NotImplementedException();
        }
    }
}