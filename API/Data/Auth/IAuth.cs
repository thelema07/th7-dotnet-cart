using System.Threading.Tasks;
using API.Models;

namespace API.Data.Auth
{
    public interface IAuth
    {
        Task<User> Register(User user, string password);
        Task<User> Login(string username, string password);
        Task<bool> UserExists(string username);
    }
}