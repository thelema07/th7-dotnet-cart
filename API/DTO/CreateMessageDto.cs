using System;

namespace API.DTO
{
    public class CreateMessageDto
    {
        public Guid SenderId { get; set; }
        public Guid RecipientId { get; set; }
        public string Content { get; set; }
        public DateTime MessageSent { get; set; }

        public CreateMessageDto()
        {
            MessageSent = DateTime.Now;
        }
    }
}