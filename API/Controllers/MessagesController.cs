using System;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Data.Messages;
using API.Data.Users;
using API.DTO;
using API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly IMessages _messages;
        private readonly Users _users;
        private readonly IMapper _mapper;
        public MessagesController(Messages messages, Users users, IMapper mapper)
        {
            _mapper = mapper;
            _users = users;
            _messages = messages;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetMessages()
        {
            var messages = await _messages.GetMessages();

            if (messages == null) return NotFound();

            return Ok(messages);
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMessage(Guid id)
        {
            var message = await _messages.GetMessage(id);

            if (message == null) return null;

            return Ok(message);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostMessage(Guid userId, CreateMessageDto createMessageDto)
        {
            // identify sender or return unauthorized
            var sender = await _users.GetUser(userId);

            if (sender.Id != Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            createMessageDto.SenderId = userId;

            var recipient = await _users.GetUser(createMessageDto.RecipientId);

            if (recipient == null)
                return BadRequest("Unable to find user");

            var message = _mapper.Map<Message>(createMessageDto);

            _users.Add(message);

            if (await _users.SaveAll())
            {
                // Must implement route redirection
                return Ok();
            }

            throw new Exception("Unable to create message");
        }
    }
}