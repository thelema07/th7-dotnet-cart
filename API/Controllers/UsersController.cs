using System;
using System.Threading.Tasks;
using API.Data.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsers _users;
        public UsersController(IUsers users)
        {
            _users = users;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            var usersFromDb = await _users.GetUsers();

            return Ok(usersFromDb);
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(Guid id)
        {
            var user = await _users.GetUser(id);

            if (user == null) return NotFound();

            return Ok(user);
        }
    }
}